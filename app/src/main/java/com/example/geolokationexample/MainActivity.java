package com.example.geolokationexample;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "my_tag";

    boolean permissions;

    TextView textView;
    Button btn;

    double x, y;
    LocationManager locationManager;
    LocationListener locationListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.main_text);
        btn = findViewById(R.id.main_button);

        grantPermition();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn.setText("Longitude: " + String.valueOf(x) + " \nLattitude: " + String.valueOf(y));
                //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
            }
        });

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                x = location.getLongitude();
                y = location.getLatitude();
                textView.setText("Longitude: " + String.valueOf(x) + " \nLattitude: " + String.valueOf(y));
                Log.d("my_tag", "Location found");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            {
                Log.d("my_tag", "Provider is on");
            }

            public void onProviderDisabled(String provider) {
                Log.d("my_tag", "Provider is off");
            }
        };

// Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


       /* locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                x = location.getLongitude();
                y = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("my_tag", "Provider is on");
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.d("my_tag", "Provider is off");
            }
        };

        Log.d("my_tag", "Permissions: "+ permissions);
        if (permissions) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
        }*/
    }

    private void grantPermition(){
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if(granted){
                        Log.d(TAG, "permissions were granted");

                    }
                    else {
                        Log.d(TAG, "permissions were denied");

                    }
                });
    }




}
