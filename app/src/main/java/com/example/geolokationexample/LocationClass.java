package com.example.geolokationexample;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class LocationClass implements LocationListener {

    Context context;
    TextView textView;

    public LocationClass(Context context, TextView textView) {
        this.context = context;
        this.textView = textView;
    }

    @Override
    public void onLocationChanged(Location location) {

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        String result ="Latitude: "+ latitude+ "\nLongitude: "+ longitude;
        textView.setText("Location: \n"+ result);


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

        Toast.makeText(context, "This provedes is disabled", Toast.LENGTH_SHORT).show();

    }
}
